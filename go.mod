module codeberg.org/anbraten/hetzner-dyndns-translator

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/hetznercloud/hcloud-go v1.32.0
	github.com/rs/zerolog v1.25.0
)
